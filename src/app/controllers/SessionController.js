const { Client } = require('../models');

class SessionController {
    async store(req, res) {
        const { email, password } = req.body;

        const client = await Client.findOne({ where: { email: req.body.email } });

        if(!client) {
            return res.status(401).json({ message: 'Client not found' })
        }

        if(!(await client.checkPassword(password))) {
            return res.status(401).json({ message: 'Invalid Password' })
        }

        return res.json({ 
            client,
            token: client.generateToken()
        });
    }
}

module.exports = new SessionController();