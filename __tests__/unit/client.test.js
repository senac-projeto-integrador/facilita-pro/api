const bcrypt = require('bcryptjs');

const { Client } = require('../../src/app/models');
const truncate = require('../utils/truncate');

describe('Client', () => {
    beforeEach(async () => {
        await truncate();
    })

    it('should encrypt client password', async () => {
        const client = await Client.create({
            name: "Max",
            lastName: "max",
            birth: '1992/11/07',
            cpf: "234324345",
            phone: "5345645656",
            email: "max@gmail.com",
            password: "123123434",
        })

        const compareHash = await bcrypt.compare('123123434', client.passwordHash);

        expect(compareHash).toBe(true);
    })
})