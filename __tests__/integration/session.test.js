const request = require('supertest');

const app = require('../../src/app')
const truncate = require('../utils/truncate');
const factory = require('../factories');

describe('Authentication', () => {
  beforeEach(async () => {
    await truncate();
  })

  it("should authenticate with valid credentials", async () => {
    const client = await factory.create('Client', {
      password: '123123434'
    })

    const response = await request(app)
      .post("/sessions")
      .send({
        email: client.email,
        password: "123123434"
      });

      expect(response.status).toBe(200);
  });

  it("should authenticate with invalid credentials", async () => {
    const client = await factory.create('Client', {
      password: '123123434'
    })

    const response = await request(app)
      .post("/sessions")
      .send({
        email: client.email,
        password: "123"
      });

      expect(response.status).toBe(401);
  });

  it("should return jwt token when authenticated", async () => {
    const client = await factory.create('Client', {
      password: '123123434'
    })

    const response = await request(app)
      .post("/sessions")
      .send({
        email: client.email,
        password: "123123434",
      });

      expect(response.body).toHaveProperty('token');
  });

  it("should return jwt token when authenticated", async () => {
    const client = await factory.create('Client', {
      password: '123123434'
    })

    const response = await request(app)
      .get("/dashboard")
      .set("Authorization", `Bearer ${client.generateToken()}`);

      expect(response.status).toBe(200);
  });

  it("should not be able to access routes without jwt token", async () => {
    const client = await factory.create('Client', {
      password: '123123434'
    })

    const response = await request(app)
      .get("/dashboard");

      expect(response.status).toBe(401);
  });

  it("should not be able to access routes with invalid token", async () => {
    const response = await request(app)
      .get("/dashboard")
      .set("Authorization", `Bearer 123123`)

      expect(response.status).toBe(401);
  });
  
})